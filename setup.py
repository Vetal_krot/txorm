from setuptools import setup


setup(
    name='txorm',
    version='0.1a2',
    packages=['txorm', 'txorm.tests', 'txorm.models',
              'txorm.models.tests', 'txorm.models.fields',
              'txorm.models.fields.tests', 'txorm.backends',
              'txorm.backends.sqlite3', 'txorm.backends.sqlite3.tests',
              'txorm.backends.postgresql', 'txorm.interfaces',
              'txorm.expressions', 'txorm.expressions.tests'],
    url='https://bitbucket.org/Vetal_krot/txorm',
    license='GPLv2',
    author='Vetal_krot',
    author_email='VetaLkrot@gmail.com',
    description='Non blocking ORM for Twisted framework',
    install_requires=[
        'Twisted',
        'Shapely',
    ]
)
