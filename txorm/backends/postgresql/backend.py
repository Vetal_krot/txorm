# -*-coding:utf-8-*-
import psycopg2
from psycopg2.extensions import register_adapter, Binary, AsIs
from twisted.internet import defer
from shapely import geometry
from txorm.backends.backend import AbstractBackend
from txorm.backends.postgresql.compiler import PGCompiler


psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)


def gis_adaptor(value):
    return AsIs('ST_GeomFromWKB({0}, {1})'.format(Binary(value.wkb), 4326))


register_adapter(geometry.GeometryCollection, gis_adaptor)
register_adapter(geometry.Point, gis_adaptor)
register_adapter(geometry.LineString, gis_adaptor)
register_adapter(geometry.Polygon, gis_adaptor)
register_adapter(geometry.MultiPoint, gis_adaptor)
register_adapter(geometry.MultiLineString, gis_adaptor)
register_adapter(geometry.MultiPolygon, gis_adaptor)


class PostgresSQLBackend(AbstractBackend):
    db_driver = 'psycopg2'
    support_returning = True
    support_for_update = True
    compiler = PGCompiler()

    def db_insert(self, sql, params, fetch_result=True):
        return self.pool.runQuery(sql, params)

try:
    from txpostgres import txpostgres, reconnection
except ImportError:
    pass
else:

    class AutoConnectionTxPostgresPool(txpostgres.ConnectionPool):

        def connectionFactory(self, reactor=None, cooperator=None, detector=None):
            if not detector:
                detector = reconnection.DeadConnectionDetector()
            return super(AutoConnectionTxPostgresPool, self).connectionFactory(reactor, cooperator, detector)


    class TxPostgresBackend(PostgresSQLBackend):
        connection_options = {}
        connection_pool_factory = AutoConnectionTxPostgresPool

        def execute_transaction(self, query_list):
            @defer.inlineCallbacks
            def execute_queries(cursor):
                results = []
                for sql, params, fetch_result in query_list:
                    yield cursor.execute(sql, params)
                    if fetch_result:
                        res = yield cursor.fetchall()
                        results.append(res)
                defer.returnValue(results)
            return self.pool.runInteraction(execute_queries)


__author__ = 'Vetal_krot'
