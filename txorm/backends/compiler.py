# -*-coding:utf-8-*-
from zope.interface import implementer
from txorm.interfaces.compiler import ICompiler


class _BaseQueryCompiler(object):
    db_operation = ''
    strategy = ()

    def __init__(self, container, compiler):
        self.container = container
        self.compiler = compiler
        self.sql = [self.db_operation]
        self.params = []

    def compile(self):
        for compiler in self.strategy:
            sql, params = getattr(self, 'compile_' + compiler)()
            if sql:
                self.sql.append(sql)
            if params:
                self.params.extend(params)
        return ' '.join(self.sql), self.params

    def compile_expression(self, expression):
        return expression.compile(self.compiler)


class WhereMixin(object):

    def compile_where(self):
        where = self.container.get_where()
        if where:
            sql, params = self.compile_expression(where)
            return 'WHERE ' + sql, params
        return None, None


class BaseSelectCompiler(_BaseQueryCompiler, WhereMixin):
    db_operation = 'SELECT'
    strategy = (
        'distinct',
        'fields',
        'from',
        'join',
        'where',
        'group_by',
        'having',
        'ordering',
        'limit',
        'offset',
    )

    def compile_fields(self):
        return self.compile_expression(self.container.get_fields())

    def compile_from(self):
        sql, params = self.compile_expression(self.container.get_table())
        return 'FROM ' + sql, params

    def compile_join(self):
        join = self.container.get_join()
        if join:
            return self.compile_expression(join)
        return None, None

    def compile_distinct(self):
        distinct = self.container.get_distinct()
        if distinct:
            sql, params = self.compile_expression(distinct)
            return 'DISTINCT ON ({0})'.format(sql), params
        return None, None

    def compile_group_by(self):
        group_by = self.container.get_group_by()
        if group_by:
            sql, params = self.compile_expression(group_by)
            return 'GROUP BY ' + sql, params
        return None, None

    def compile_having(self):
        having = self.container.get_having()
        if having:
            sql, params = self.compile_expression(having)
            return 'HAVING ' + sql, params
        return None, None

    def compile_limit(self):
        if self.container.get_limit():
            return 'LIMIT {0}'.format(self.container.get_limit()), None
        return None, None

    def compile_offset(self):
        if self.container.get_offset():
            return 'OFFSET {0}'.format(self.container.get_offset()), None
        return None, None

    def compile_ordering(self):
        order_by = self.container.get_order_by()
        if order_by:
            sql, params = self.compile_expression(order_by)
            return 'ORDER BY ' + sql, params
        return None, None


class BaseInsertCompiler(_BaseQueryCompiler):
    db_operation = 'INSERT'
    strategy = (
        'into',
        'fields',
        'from_select',
        'values',
    )

    def compile_into(self):
        sql, params = self.compile_expression(self.container.get_table())
        return 'INTO ' + sql, params

    def compile_fields(self):
        sql, params = self.compile_expression(self.container.get_fields())
        return '({0})'.format(sql), params

    def compile_values(self):
        values = self.container.get_values()
        if values:
            sql, params = self.compile_expression(values)
            return 'VALUES ' + sql, params
        return None, None

    def compile_from_select(self):
        select = self.container.get_values_select()
        if select:
            return self.compile_expression(select)
        return None, None


class BaseUpdateCompiler(_BaseQueryCompiler, WhereMixin):
    db_operation = 'UPDATE'
    strategy = (
        'table',
        'set_values',
        'where',
    )

    def compile_table(self):
        return self.compile_expression(self.container.get_table())

    def compile_set_values(self):
        sql, params = self.compile_expression(self.container.get_update())
        return 'SET ' + sql, params


class BaseDeleteCompiler(_BaseQueryCompiler, WhereMixin):
    db_operation = 'DELETE'
    strategy = (
        'table',
        'where',
    )

    def compile_table(self):
        sql, params = self.compile_expression(self.container.get_table())
        return 'FROM ' + sql, params


@implementer(ICompiler)
class SQLCompiler(object):
    formatter = '%s'
    expression_scheme = {}
    insert_compiler = BaseInsertCompiler
    select_compiler = BaseSelectCompiler
    update_compiler = BaseUpdateCompiler
    delete_compiler = BaseDeleteCompiler

    def _compile(self, container, compiler):
        return compiler(container, self).compile()

    def compile_insert(self, container):
        return self._compile(container, self.insert_compiler)

    def compile_select(self, container):
        return self._compile(container, self.select_compiler)

    def compile_update(self, container):
        return self._compile(container, self.update_compiler)

    def compile_delete(self, container):
        return self._compile(container, self.delete_compiler)

    def get_expression(self, expr):
        return self.expression_scheme.get(expr)


__author__ = 'Vetal_krot'
