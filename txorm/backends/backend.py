# -*-coding:utf-8-*-
from zope.interface import implementer
from twisted.internet import defer
from twisted.enterprise import adbapi

from txorm.interfaces.backend import IBackend
from txorm.query import Select, Insert, Update, Delete


@implementer(IBackend)
class AbstractBackend(object):
    db_driver = ''
    connection_pool_factory = adbapi.ConnectionPool
    support_returning = False
    support_for_update = False
    compiler = None
    connection_options = {
        'cp_reconnect': True
    }

    def __init__(self, database, user, password, host, port=None, **options):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.options = options
        self.pool = self.connection_pool_factory(self.get_db_driver_name(), **self.get_connection_params())

    def get_db_driver_name(self):
        return self.db_driver

    def get_connection_params(self):
        connection_params = self.connection_options.copy()
        connection_params.update(self.options)
        connection_params['database'] = self.database
        connection_params['user'] = self.user
        connection_params['password'] = self.password
        connection_params['host'] = self.host
        connection_params['port'] = self.port
        return connection_params

    def _build_query(self, query_factory, *args):
        query = query_factory(*args)
        query.set_backend(self)
        return query

    def insert(self, *args):
        return self._build_query(Insert, *args)

    def select(self, *args):
        return self._build_query(Select, *args)

    def update(self, *args):
        return self._build_query(Update, *args)

    def delete(self, *args):
        return self._build_query(Delete, *args)

    def _execute_query(self, sql, params, fetch_result):
        if fetch_result:
            return self.pool.runQuery(sql, params)
        return self.pool.runOperation(sql, params)

    def db_insert(self, sql, params, fetch_result=True):
        def insert_and_return_pk(cursor):
            cursor.execute(sql, params)
            return [(cursor.lastrowid,)]
        return self.pool.runInteraction(insert_and_return_pk)

    def db_select(self, sql, params, fetch_result=True):
        return self.pool.runQuery(sql, params)

    def db_update(self, sql, params, fetch_result=False):
        return self._execute_query(sql, params, fetch_result)

    def db_delete(self, sql, params, fetch_result=False):
        return self._execute_query(sql, params, fetch_result)

    def execute_transaction(self, query_list):
        def execute_queries(cursor):
            results = []
            for sql, params, fetch_result in query_list:
                cursor.execute(sql, params)
                if fetch_result:
                    results.append(cursor.fetchall())
            return results
        return self.pool.runInteraction(execute_queries)

    def get_compiler(self):
        return self.compiler

    def start(self):
        d = defer.maybeDeferred(self.pool.start)
        d.addCallback(lambda res: self)
        return d

    def stop(self):
        d = defer.maybeDeferred(self.pool.close)
        d.addCallback(lambda res: self)
        return d


__author__ = 'Vetal_krot'
