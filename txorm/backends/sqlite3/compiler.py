# -*-coding:utf-8-*-
from txorm.backends.compiler import SQLCompiler, BaseInsertCompiler


class SQLite3InsertCompiler(BaseInsertCompiler):

    def compile_values(self):
        values = self.container.get_values()
        if values:
            vals = values.values
            p = 'VALUES ({0})'
            sql = p.format(', '.join([self.compiler.formatter] * len(vals[0])))
            if len(vals) > 1:
                params = map(tuple, vals)
            else:
                params = vals[0]
            return sql, params
        return None, None


class SQLite3Compiler(SQLCompiler):
    formatter = '?'
    # insert_compiler = SQLite3InsertCompiler