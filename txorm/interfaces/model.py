# -*-coding:utf-8-*-
from zope.interface import Interface, Attribute


class IModelOptions(Interface):
    """
    Container for data specify with model
    """
    name = Attribute('Model name')
    db_table = Attribute('Model name in db')
    fields = Attribute('List of model fields. Field - implementer `IField`')
    relations = Attribute('List of relations with model')
    pk_field = Attribute('Primary key field for model')


class IModel(Interface):
    """
    Python abstraction over db model
    """
    _meta = Attribute('Implementer of `IModelOptions`')
    pk = Attribute('Primary key value')
    manager = Attribute('ORM request builder. Implementer of `IManager`')

    def get_meta():
        """
        Get for self._meta
        """

    def save():
        """
        Save values to model

        :return: defer.Deferred
        """

    def delete():
        """
        Delete data from db

        :return: defer.Deferred
        """


__author__ = 'Vetal_krot'
