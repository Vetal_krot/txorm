# -*-coding:utf-8-*-
from zope.interface import Interface


class ICompilable(Interface):
    """
    Interface for object who can compiled to sql query
    """

    def compile(compiler):
        """
        Compile this object to sql query

        :param compiler: implementer if `ICompiler`
        :return: sql, params
        """


class ICompiler(Interface):
    """
    SQL compiler.
    """

    def compile_insert(container):
        """
        Compile query to SQL INSERT

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    def compile_select(container):
        """
        Compile query to SQL SELECT

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    def compile_update(container):
        """
        Compile query to SQL UPDATE

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    def compile_delete(container):
        """
        Compile query to SQL DELETE

        :param container:  implementer of `ICompiled`, `IQueryContainer`
        :return: sql, args
        """

    def get_expression_pattern(expr):
        """
        Get db expression pattern

        :param expr: python expressions name
        :return: sql expression pattern
        :rtype: str
        """

    def get_formatter(self):
        """
        Get specific db driver formatter
        :return: formatter f ex (? or %s)
        """

__author__ = 'Vetal_krot'
