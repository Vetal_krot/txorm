# -*-coding:utf-8-*-
from zope.interface import Interface


class IRouter(Interface):
    """
    DB router

    contains IBackend implementers and return his for specific actions
    """

    def for_delete(model):
        """
        get backend for db delete

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

    def for_update(model):
        """
        get backend for db update

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

    def for_select(model):
        """
        get backend for db select

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

    def for_insert(model):
        """
        get backend for db insert

        :param model: implementer `IModel`
        :return: implementer `IBackend`
        """

__author__ = 'Vetal_krot'
