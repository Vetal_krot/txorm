# -*-coding:utf-8-*-
from zope.interface import implementer
from txorm.interfaces.model import IModel
from txorm.interfaces.compiler import ICompilable
from txorm.interfaces.field import IField
from txorm.expressions.operator import Node
from txorm.expressions.util import compile_iterable


__all__ = ['COUNT', 'SUM', 'MIN', 'MAX', 'AVG', 'Replace', 'Lower', 'Upper']

@implementer(ICompilable)
class SQLFunctionBase(Node):
    func_name = None

    def __init__(self, *params):
        self.params = params

    def get_params(self):
        return self.params

    def get_func_name(self):
        return self.func_name or self.__class__.__name__.lower()

    def compile(self, compiler):
        sql, params = compile_iterable(self.get_params(), compiler)
        if isinstance(sql, list):
            sql = u', '.join(sql)
        return u'{0}({1})'.format(self.get_func_name(), sql), params


@implementer(ICompilable)
class AggregateSQLFuncBase(object):
    sql_func = ''

    def __init__(self, field):
        self.field = field

    def get_args(self):
        if IField.providedBy(self.field):
            return self.field.get_sqlname()
        return self.field

    def compile(self, compiler):
        return u'{0}({1})'.format(self.sql_func, self.get_args()), None


class COUNT(AggregateSQLFuncBase):
    sql_func = u'COUNT'

    def __init__(self, table=None, field=None):
        AggregateSQLFuncBase.__init__(self, field)
        self.table = table

    def get_args(self):
        if IField.providedBy(self.field):
            return self.field.get_sqlname()
        if IModel.implementedBy(self.table):
            return self.table.pk.get_sqlname()
        field = self.field or u'*'
        if self.table:
            return u'"{0}".{1}'.format(self.table, field)
        return field


class MAX(AggregateSQLFuncBase):
    sql_func = u'MAX'


class MIN(AggregateSQLFuncBase):
    sql_func = u'MIN'


class SUM(AggregateSQLFuncBase):
    sql_func = u'SUM'


class AVG(AggregateSQLFuncBase):
    sql_func = u'AVG'


class Replace(SQLFunctionBase):
    pass


class Lower(SQLFunctionBase):
    pass


class Upper(SQLFunctionBase):
    pass


__author__ = 'Vetal_krot'
