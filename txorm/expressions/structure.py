from txorm.interfaces.query import IQueryBuilder
from zope.interface import implementer
from txorm.interfaces.field import IField
from txorm.interfaces.model import IModel
from txorm.interfaces.compiler import ICompilable
from txorm.expressions.util import compile_value, compile_iterable


@implementer(ICompilable)
class JoinExpressions(object):
    delimiter = ' '

    def __init__(self, values):
        self.values = values

    def compile(self, compiler):
        sql, params = compile_iterable(self.values, compiler)
        return self.delimiter.join(sql), params


class ValuesList(JoinExpressions):
    def compile(self, compiler):
        sql, params = [], []
        for values in self.values:
            _sql, _params = compile_iterable(values, compiler)
            sql.append(', '.join(_sql))
            params.extend(_params)
        return '({0})'.format('), ('.join(sql)), params


class FieldsList(JoinExpressions):
    delimiter = ', '

    def compile(self, compiler):
        sql, params = [], []
        for value in self.values:
            if ICompilable.providedBy(value):
                _sql, _params = compile_value(value, compiler)
                if _sql:
                    sql.append(_sql)
                if _params:
                    params.extend(_params)
            elif IField.providedBy(value):
                sql.append(value.get_sqlname())
            else:
                sql.append(value)
        return self.delimiter.join(sql), params


@implementer(ICompilable)
class SetValue(object):
    def __init__(self, key, value):
        self.key = key
        self.value = value

    def compile(self, compiler):
        if IField.providedBy(self.key):
            field_name = self.key.get_sqlname(False)
        else:
            field_name = self.key
        sql, params = compile_value(self.value, compiler)
        return '{0} = {1}'.format(field_name, sql), params


@implementer(ICompilable)
class AS(object):
    def __init__(self, field, as_field):
        self.field = field
        self.as_field = as_field

    def compile(self, compiler):
        sql, params = compile_value(self.field, compiler)
        return '{0} AS "{1}"'.format(sql, self.as_field), params


@implementer(ICompilable)
class ASC(object):
    def __init__(self, field):
        self.field = field

    def compile(self, compiler):
        params = None
        if IField.providedBy(self.field):
            sql = self.field.get_sqlname()
        elif ICompilable.providedBy(self.field):
            sql, params = self.field.compile(compiler)
            if IQueryBuilder.providedBy(self.field):
                sql = '({0})'.format(sql)
        else:
            sql = self.field
        return '{0} {1}'.format(sql, self.__class__.__name__), params


class DESC(ASC):
    pass


@implementer(ICompilable)
class Join(object):
    pattern = 'INNER JOIN "{0}" ON ({1})'

    def __init__(self, table, on):
        self.table = table
        self.on = on

    def get_table_name(self):
        if IModel.implementedBy(self.table):
            return self.table.get_meta().get_tablename()
        return self.table

    def compile(self, compiler):
        sql, params = compile_value(self.on, compiler)
        return self.pattern.format(self.get_table_name(), sql), params


class LeftJoin(Join):
    pattern = 'LEFT OUTER JOIN "{0}" ON ({1})'


class RightJoin(Join):
    pattern = 'RIGHT OUTER JOIN "{0}" ON ({1})'