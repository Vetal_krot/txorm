from txorm.constants import EXPR_DELIMITER
from txorm.interfaces.field import IField
from txorm.interfaces.query import IQueryBuilder
from txorm.interfaces.compiler import ICompilable


def expression_from_string(model, expr_str, value):
    expr_name = 'equals'
    names = expr_str.split(EXPR_DELIMITER, 1)
    field_name = names.pop(0)
    field = getattr(model, field_name)
    if not field:
        raise AttributeError("Model {0} not have field {1}".format(
            model, field_name))
    if names:
        expr_name = names[0]
    return getattr(field, expr_name)(value)


def compile_value(value, compiler):
    sql, params = None, None
    if IField.providedBy(value):
        sql = value.get_sqlname()
    elif ICompilable.providedBy(value):
        sql, params = value.compile(compiler)
        if IQueryBuilder.providedBy(value):
            sql = '({0})'.format(sql)
    else:
        sql, params = compiler.formatter, [value]
    return sql, params


def compile_iterable(values, compiler):
    sql, params = [], []
    for value in values:
        _sql, _params = compile_value(value, compiler)
        if _sql:
            sql.append(_sql)
        if _params:
            params.extend(_params)
    return sql, params