from twisted.trial import unittest
from txorm.models.model import Model
from txorm.models.fields.related import ForeignKey


class Model1(Model):
    class Meta:
        db_table = 'table1'


class Model2(Model):
    class Meta:
        db_table = 'table2'

    model1 = ForeignKey(Model1, related_name='models2')


class ForeignKeyTest(unittest.TestCase):

    def test_field(self):
        self.assertEqual(Model2, Model2.model1.model)
        self.assertEqual(Model1, Model2.model1.related_model)
        self.assertEqual(Model1.pk, Model2.model1.related_field)

    def test_self_relation(self):
        relation = Model2.model1.relation
        self.assertEqual(Model2, relation.model)
        self.assertEqual(Model2.model1, relation.field)
        self.assertEqual(Model1, relation.related_model)
        self.assertEqual(Model1.pk, relation.related_field)

    def test_reverse_relation(self):
        relation = Model1.models2
        self.assertEqual(Model1, relation.model)
        self.assertEqual(Model1.pk, relation.field)
        self.assertEqual(Model2, relation.related_model)
        self.assertEqual(Model2.model1, relation.related_field)