# -*-coding:utf-8-*-
from shapely import wkb, wkt, geometry
from txorm.models.fields.base import Field
from txorm.expressions.gis import GisNode


__all__ = ['GeometryField', 'GeometryCollectionField', 'PointField',
           'LineStringField', 'PolygonField', 'MultiPointField',
           'MultiLineStringField', 'MultiPolygonField', 'D']


class Distance(object):

    def __init__(self, km=0, m=0, cm=0, spheroid=True):
        self.spheroid = spheroid
        self.km = float(km)
        self.m = float(m)
        self.cm = float(cm)

    def to_meters(self):
        return self.km / 1000 + self.m + self.cm * 1000

    def __str__(self):
        return self.to_meters()


D = Distance


class GeometryField(GisNode, Field):
    geom = None

    def __init__(self, srid=4326, *args, **kwargs):
        Field.__init__(self, *args, **kwargs)
        self.srid = srid
        if self.blank:
            self.null = True

    @property
    def geom_type(self):
        return self.geom.__class__.__name__.upper()

    def encoder(self, value):
        if isinstance(value, (tuple, list)):
            value = self.geom(value)
        elif isinstance(value, (str, unicode)):
            if value.startswith(self.geom_type):
                value = wkt.loads(value)
            else:
                value = wkb.loads(value, hex=True)
        return value

    def decoder(self, value):
        return wkb.loads(value, hex=True)


class GeometryCollectionField(GeometryField):
    geom = geometry.GeometryCollection


class PointField(GeometryField):
    geom = geometry.Point


class LineStringField(GeometryField):
    geom = geometry.LineString


class PolygonField(GeometryField):
    geom = geometry.Polygon


class MultiPointField(GeometryCollectionField):
    geom = geometry.MultiPoint


class MultiLineStringField(GeometryCollectionField):
    geom = geometry.MultiLineString


class MultiPolygonField(GeometryCollectionField):
    geom = geometry.MultiPolygon
