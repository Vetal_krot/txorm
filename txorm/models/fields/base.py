# -*-coding:utf-8-*-
from txorm.interfaces.node import INode
from zope.interface import implementer
from txorm.expressions import Node

from txorm.interfaces.field import IField
from txorm.models.fields.validators import (MaxLengthValidator,
    AllowedValuesValidator, NotNullValidator, ValidationError)


class Empty(object):
    def __new__(cls, *args, **kwargs):
        return cls


EMPTY_VALUES = (Empty, None, '')


@implementer(IField)
class BaseField(object):
    abstract = False
    mutable = False

    def __init__(self, primary_key=False, max_length=None, unique=False,
                 blank=False, null=False, db_index=False, default=Empty,
                 db_column=None, validators=None, choices=None, **kwargs):
        self.model = None
        self.primary_key = primary_key
        self.max_length = max_length
        self.unique = unique
        self.blank = blank
        self.null = null
        self.db_index = db_index
        self.default = default
        self.db_column = db_column
        self.choices = choices
        self.validators = validators or []
        self.attrname = None
        self.init_validators()

    def init_validators(self):
        if not self.null:
            self.validators.append(NotNullValidator())
        if self.max_length:
            self.validators.append(MaxLengthValidator(self.max_length))
        if self.choices:
            allowed_values = [v[0] for v in self.choices]
            self.validators.append(AllowedValuesValidator(allowed_values))

    def get_copy_kwargs(self):
        return dict(max_length=self.max_length, unique=self.unique,
                    blank=self.blank, null=self.null, default=self.default,
                    choices=self.choices)

    def copy(self):
        return self.__class__(**self.get_copy_kwargs())

    @property
    def db_table(self):
        return self.model.get_meta().db_table

    def set_model(self, model):
        self.model = model

    def get_sqlname(self, with_db_table=True):
        if not with_db_table:
            return '"{0}"'.format(self.db_column)
        return '"{0}"."{1}"'.format(self.db_table, self.db_column)

    def validate(self, value):
        for validator in self.validators:
            try:
                validator(value)
            except Exception as err:
                raise ValidationError(
                    u"Field '{0}' at model {1}.\r\nValidator {2}: '{3}'".format(
                        self.get_attrname(), self.model, validator, err))

    def encoder(self, value):
        return value

    def to_sql(self, value):
        if value is None and self.null:
            return None
        if value in EMPTY_VALUES and self.has_default():
            return self.get_default()
        if value is not None:
            value = self.encoder(value)
        self.validate(value)
        return value

    def decoder(self, value):
        return value

    def to_python(self, value):
        if value is None and self.null:
            return None
        return self.decoder(value)

    def set_attrname(self, name):
        self.attrname = name
        if not self.db_column:
            self.db_column = name

    def get_attrname(self):
        return self.attrname

    def has_default(self):
        return self.default is not Empty

    def get_default(self):
        if not self.has_default():
            raise AttributeError(
                "Field '{0}' in model '{1}' not have default value.".format(
                    self.get_attrname(), self.model))
        if callable(self.default):
            return self.default()
        return self.default


class Field(BaseField, Node):
    pass


@implementer(INode, IField)
class FieldWrapper(object):

    def __init__(self, field):
        self.field = field

    def to_python(self, value):
        return self.field.to_python(value)

    def to_sql(self, value):
        return self.field.to_sql(value)

    def copy(self):
        return self.field.copy()

    @property
    def db_table(self):
        return self.model.get_meta().db_table

    @property
    def db_column(self):
        return self.field.db_column

    def set_model(self, model):
        self.field.set_model(model)

    def get_sqlname(self, *args, **kwargs):
        return self.field.get_sqlname(*args, **kwargs)

    def set_attrname(self, name):
        self.field.set_attrname(name)

    def get_attrname(self):
        return self.field.get_attrname()

    def has_default(self):
        return self.field.has_default()

    def get_default(self):
        return self.field.get_default()

    def equals(self, other):
        return self.field.equals(other)

    def not_equals(self, other):
        return self.field.not_equals(other)

    def less(self, other):
        return self.field.less(other)

    def less_or_equals(self, other):
        return self.field.less_or_equals(other)

    def great(self, other):
        return self.field.great(other)

    def great_or_equals(self, other):
        return self.field.great_or_equals(other)

    def in_list(self, other):
        return self.field.in_list(other)

    def not_in_list(self, other):
        return self.field.not_in_list(other)

    def between(self, *args, **kwargs):
        return self.field.between(*args, **kwargs)

    def not_between(self, *args, **kwargs):
        return self.field.not_between(*args, **kwargs)

    def like(self, other):
        return self.field.like(other)

    def not_like(self, other):
        return self.field.not_like(other)

    def ilike(self, other):
        return self.field.ilike(other)

    def not_ilike(self, other):
        return self.field.not_ilike(other)

    def contains(self, other):
        return self.field.contains(other)

    def not_contains(self, other):
        return self.field.not_contains(other)

    def startswith(self, other):
        return self.field.startswith(other)

    def not_startswith(self, other):
        return self.field.not_startswith(other)

    def endswith(self, other):
        return self.field.endswith(other)

    def not_endswith(self, other):
        return self.field.not_endswith(other)

    def icontains(self, other):
        return self.field.icontains(other)

    def not_icontains(self, other):
        return self.field.not_icontains(other)

    def istartswith(self, other):
        return self.field.istartswith(other)

    def not_istartswith(self, other):
        return self.field.not_istartswith(other)

    def iendswith(self, other):
        return self.field.iendswith(other)

    def not_iendswith(self, other):
        return self.field.not_iendswith(other)

    def concat(self, other):
        return self.field.concat(other)

    def add(self, other):
        return self.field.add(other)

    def sub(self, other):
        return self.field.sub(other)

    def mul(self, other):
        return self.field.mul(other)

    def div(self, other):
        return self.field.div(other)

    def label(self, label):
        return self.field.label(label)

    def __or__(self, other):
        return self.field.__or__(other)

    def __and__(self, other):
        return self.field.__and__(other)

    __eq__ = eq = equals
    __ne__ = ne = not_equals
    __lt__ = lt = less
    __le__ = le = less_or_equals
    __gt__ = gt = great
    __ge__ = ge = great_or_equals
    __add__ = add
    __sub__ = sub
    __mul__ = mul
    __div__ = div


class PrimaryKeyAbstractField(FieldWrapper):
    abstract = True

    def __get__(self, instance, owner):
        if instance:
            return getattr(instance, self.field.get_attrname())
        return self.field

    def __set__(self, instance, value):
        setattr(instance, self.field.get_attrname(), value)

    def __delete__(self, instance):
        delattr(instance, self.field.get_attrname())