# -*-coding:utf-8-*-
from zope.interface import implementer
from txorm.interfaces.router import IRouter
from txorm.models.manager import Manager


def register_router(router):
    Manager.set_router(router)


@implementer(IRouter)
class AbstractRouter(object):

    def for_delete(self, model):
        raise NotImplemented()

    def for_update(self, model):
        raise NotImplemented()

    def for_select(self, model):
        raise NotImplemented()

    def for_insert(self, model):
        raise NotImplemented()


class SimpleRouter(AbstractRouter):
    def __init__(self, backend):
        self.backend = backend

    def for_delete(self, model):
        return self.backend

    def for_update(self, model):
        return self.backend

    def for_select(self, model):
        return self.backend

    def for_insert(self, model):
        return self.backend


class MasterSlaveRouter(AbstractRouter):
    def __init__(self, master, *slaves):
        self.master = master
        self.slaves = slaves
        self.next_slave = 0

    def for_insert(self, model):
        return self.master

    def for_update(self, model):
        return self.master

    def for_delete(self, model):
        return self.master

    def for_select(self, model):
        if len(self.slaves) >= self.next_slave:
            self.next_slave = 0
        s = self.slaves[self.next_slave]
        self.next_slave += 1
        return s


__author__ = 'Vetal_krot'
